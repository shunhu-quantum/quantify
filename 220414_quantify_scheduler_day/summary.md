# Quantify-scheduler day 14 April 2022

--------------------------

### Introduction: Direction towards v1.0 of `quantify-scheduler` 

-   Limited new functionality (within reason)
-   Stable and robust execution of quantum schedules
-   _Essentially, v1.0 == refactor hardware backend & be prepared for features that come after v1.0 (e.g., feedback, potentially already including fast feedback in v1.0)_
 
####   General Design Mindset
-   **Transparency** and debug-ability
	-   _E.g., it should be clear what happens at each compilation step, and the state of the program should be accessible after compilation (e.g.,_ _[hardware timing table in ZI backend](https://quantify-quantify-scheduler.readthedocs-hosted.com/en/0.6.0/tutorials/zhinst/T_verification_programs.py.html#the-hardware-timing-table)__)_
-   **Modularity**
	-   _E.g., different compilation steps are implemented as modular functions and have a modular configuration structure_
-   **KISS**
	-   _It’s hard enough as is, let’s avoid e.g. advanced language functionality when it is not strictly needed_
-   **Small improvements over huge changes/always have a working system**
	-   _We need to not only think about the final design in the ideal world, but rather of the smaller steps and the working systems on the way to get there_

### Quantify's strengths and weaknesses 
-   **_All attendees_**_: Please prepare your input beforehand!_
-   We will put everyone's contribution on whiteboard, to inspire everyone during rest of the day

![Whiteboard_Quantify_pros_cons](./quantify_pros_cons.png)


###  Whiteboard session `OUR IDEAL HARDWARE BACKEND.` 
  **a)**  Breaking up the problem in **small compilation "nodes"**  
  **b)**  Definition of **[intermediate representations](https://quantify-quantify-scheduler.readthedocs-hosted.com/en/0.6.0/user_guide.html#compilation)**  
  **c)  Modular configuration files**  
  **d)  Acquisition protocols**, **measurements** and **data interfaces**  
  **e)**  _Other subjects?_  

-   Modularity: **Only used elements** should be taken into account in the backend compilation layer (from Dev meeting)
-   Modularity: Compiling **pulse-only schedules** (requires quantum device object, feels awkward) (from Jules)

![ideal_hardware_backend.png](./ideal_hardware_backend.png)

During this session, we converged on a few ideas.

1. **Definition** of a quantify-scheduler backend.  
	- A `quantify backend` defines a directed acyclic graph (DAG) of functions that when executed fulfil the following input output requirements: 
	  - The input is a `Schedule` and a configuration file, 
	  - and the output consists of platform specific hardware instructions.  
	- A note was made that such a `quantify backend` does not include the desired transparency in the definition as intermediate representations are not included in the definition of the output. Instead, this is envisioned to be supported by providing access to the intermediate state after each node in the graph through some other means. This means that design work will be required.
 
1. Certain **edge cases** were discussed. 
	We currently do not know how we want to support these edge cases:  
	- Spectroscopy and variational quantum algorithms (VQA).  
	- Node with same input should skip and return (cached) output. This is a nice idea, but would require extensive caching.  
	- Continuously applied signals such as  
	  - DC currents or voltages  
	  - CW microwave tones  
	- Measurement abstraction. 

1. Definition of the **return data format** behaviour of a `quantify backend` which relates to Measurement abstraction above.
	- Executing a `Schedule` should return a dataset obeying quantify conventions. 
	- -> A `Schedule` should define the structure of the dataset before execution.  
	(note that this does not imply the exact shape so as to conform adaptive execution of schedules). 
​
###  Classical logic 
-   What do we need to have in mind right now so we don’t shoot ourselves in the foot (i.e., to avoid a major redesign later)
-   Primarily: impact of **classical logic** on the **intermediate representation**
-   Links back to discussions of non-timeline altering feedback/logic (**fast feedback**) and timeline breaking logic (**comprehensive feedback**)

![classical_logic.png](./classical_logic.png)

Two types of logic were discussed 
- non-timeline breaking or simple-feedback 
- timeline breaking or comprehensive feedback. 

Damaz's approach of interweaving logic instructions seems an approach that can be prototyped. 

The timing table as an intermediate representation does not need a significant redesign to support this use-case. 


# => Conclusions (at day)
1. We can **proceed with compiler ideas** from whiteboard session. (see above). 
1. The **timing table** does **not** need to be fundamentally **changed** to **accommodate classical logic**. 
1. **Short-term logic** features consists of "binary" logic and **can be prototyped**. 
1. **Longer-term logic** requires high-level programming functionality and is **out of scope for now** but may involve concepts like _kernels_. 

## Graph of current compilation

Adriaan drew a graph of the current compilation (left), and the ZI backend (right) on the whiteboard. 
![compiler.jpg](./compiler.jpg)


# => Thoughts / Follow-ups

1. **Next steps** and **responsible people** still need to be defined.
1. The devil is in the details, we **converged** on some very important **high-level** subjects but **did not go into** the detail where the **limitations for users** show clearly. 
1. A **session** needs to be planned to discuss the **measurement/acquisition model**.



