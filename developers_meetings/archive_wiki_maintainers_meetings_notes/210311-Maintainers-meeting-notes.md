Current state (Max 15 mins):
* .pylintrc config file available for quantify-core and soon to be for scheduler (!55).
  * Victor will merge open quantify-scheduler pylint config with the one from quantify-core.
  * Victor mentioned automatically sorting the imports, sometimes pylint shows a warning.
    * Skip for now due to high impact on developers workflow.
* All devs should use Pylint from now on with the suggested config file.
  * done.

    Quantify-core:
    * Analysis class addressing #63 merged. !89 (In terms of process, Adriaan worked on it. As he's also PO, I suppose he's also happy with it? Any other discussions from the contributors?)
      * Contributors keep on improving as we speak, good progress.
      * Docs are missing, needs work. #163
      * User experience needs improvement. It is not clear if errors occur, what to do.
    * Improved installation document.
      * Core and Scheduler installation docs are not aligned.

    Quantify-scheduler:
    * Progress on Acquisition Protocols. Time for review and merge as both tasks are complete. !51 (Reason for WIP tag still?)
      * Minor open issues will be resolved by Damien regarding: docstrings and some naming convention changes.
      * Can be merged soon.
    * Zurich Instruments backend also in progress. !49
      * Will be merged next week when the project ends.

Discussions:

    Quantify-core (max 20 mins):
    * Release 0.4.0 discussion. Issue #163
    * Other technical issues raised?

    Quantify-scheduler (max 20 mins):
    * The MR !51 of Acquisition protocols is hanging for far too long with large discussions. [Suggest that the extra comments from Jules be made into a separate issue and another MR be made to address it as a working implementation is already there]. The idea on sprints is that we do not dwell on design issues for too long, and instead work on it for manageable bite size pieces. 107 commits is a LOT.
    * Other technical issues raised?

AOB (5 mins):

    Process wise:
    * We will delay reflection yet again (since we missed it last week) to prep for release of core 0.4.0. But this is the last time we can delay it.
    * Victor should be backup scrum master in case Kelvin cannot make it? The role of Adriaan and Jules must be kept as Product Owners and ideally should not chair the meetings. Scrum master's role is also to evaluate if we have enough bandwidth for the whole team. Still a bit blur for Quantify as our team is still very small but it's getting there.
