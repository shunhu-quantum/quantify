This is the first meeting in the new format. 
Chair: Callum as SCRUM master 
Notes: Adriaan 

## Agenda 

* Review issue progress from past sprint 
* Evaluate progress and identify priorities next sprint 
* Any other business (optional) 

## Review issue progress 
### Process
All issues that were closed are added to a window by Callum. 
Each topic primary implementer/decision maker will present. This is the final opportunity for people to dispute the solution or note edge cases etc. 

### Issue progress 

Agenda point identified. What is the process for fixing critical issues. 

Decided not to add style guide on how to write docs as this creates a larger barrier for contributors to contribute, and forces us to maintain more code/docs. 

## Further discussion 
- We need to define the fix process. (to be discussed next meeting) 
- Adriaan will focus on drafting a release after the .batched merge on core and the resource refactor on the scheduler repo. 
