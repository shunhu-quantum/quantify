Present:

## Current state (Max 30 mins):

### Quantify-core:
* We can make it latest this Friday? V0.4.0 [Ooof that aged well...]
  - Still need to finish analysis docs
  - Backend docs almost ready - not key priority
  - Change names of dataset and processed dataset
  - Decide by end of today whether to merge tomorrow

Key issues and MRs left:
* quantify-core#177 [AFAIK, it's ready...]
* quantify-core#193
  - Addressed in merge request https://gitlab.com/quantify-os/quantify-core/-/merge_requests/167
* quantify-core#197

### Quantify-scheduler:
* We can make it latest this Friday? V0.3.0 [Ditto...]
* quantify-scheduler!81 relates to quantify-scheduler#101, quantify-scheduler#93, quantify-scheduler#86, quantify-scheduler#74, quantify-scheduler#73 and quantify-scheduler#89 already reviewed. Merge?

Key issues and MRs left:
* quantify-scheduler!98 closes quantify-scheduler#48 (In review: Need reviewers!) [Also ready with some exceptions...]
* quantify-scheduler#102
  - Not key, assign to 0.4.0
* quantify-scheduler!92


## Discussions:

- Issue with sphinx extensions for docs - autodocumenting based on type hints
  - Fix accepted in QCoDeS
  - Issue with plotly still a problem - discussion on whether to drop plotly
  - Rest of warnings fixable

## AOB (max 15 mins):
* https://unitaryfund.github.io/unitaryhack/
  - Visualisation of interpolation/optimisation analysis
  - Bounties in $25 - $75 range
* Discuss unitaryhack issues! https://gitlab.com/quantify-os/quantify/-/issues/11

- At risk of missing merge requests by external contributors
  - Maintainers can assign people to review and reply to contributor
  - Need to decide whether to ignore or deal with MR
- Adopt new json representation for jupyter lab