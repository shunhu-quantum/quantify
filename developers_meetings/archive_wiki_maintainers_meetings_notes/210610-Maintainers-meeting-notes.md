Present:

## Current state (Max 30 mins):

### Quantify-core:

It's been 1 month (May 10th) since last release.

Proposed new Milestone: 

* quantify-core%8
* Due date: **June 25th**
* Kelvin and Adam, is it doable?
  - Decided to implement by 25th

### Quantify-scheduler:

(Slow) progress on quantify-scheduler%2

- ControlStack class merged quantify-scheduler!71
- Working on the ControlStack components quantify-scheduler!99, quantify-scheduler!112.
  - Next week: discuss how to link drivers with components
  - Thomas leaving 1st July, figure out how to manage transition
  - Many people on holiday in August
  - Next week: discuss splitting up milestones

Next steps: 

* Review and merge ControlStack components.
* Converge on the how to link Instrument drivers with each ControlStack components (discussion in quantify-scheduler!71 and follow up issues).

Discussion:

* Maintainers have been busy and delayed on the reviews.
* Any comments? Anything we could do better to speed up this?
* Would it be useful to create smaller intermediate milestones?

## Requested discussions:

* \[2021-05-26, Requested by Kelvin\] [quantify-scheduler#66](/quantify-os/quantify-scheduler/-/issues/66)
* \[2021-06-08, Requested by Victor\] Decide approval of [quantify-core!186](/quantify-os/quantify-core/-/merge_requests/186)

## AOB (max 5 mins):
* When you build the docs, only public methods are documented.
  - Decided to build private methods in CI, and have option to build locally