Present:

## Current state (Max 30 mins):

### Quantify-core:
* We can make it latest next Wednesday! V0.4.0
* Closed quantify-core!162, quantify-core!156
* Readme has a badge which shows we're also supported by the Unitary Fund: ![image](uploads/b64c6c6ccbf1993b08e095f366c2ea87/image.png)

Key issues left:
* quantify-core#177
* New analysis.run() method for defining optional arguments instead of overriding init 


### Quantify-scheduler:
* We can make it latest next Wednesday! V0.3.0
* Merged quantify-scheduler!83 but some lessons to be learnt. Discuss validators and the if-else construct.
  - Use validators instead of if-else statments
  - Call them validators instead of vals
* Blocked MR: quantify-scheduler!81 relates to quantify-scheduler#101, quantify-scheduler#93, quantify-scheduler#86, quantify-scheduler#74, quantify-scheduler#73 and quantify-scheduler#89

Key issues left:
* quantify-scheduler#48 (Should be done by tomorrow)
* quantify-scheduler#102


## Discussions:

## General process MR (max 15 mins):
Victor and Kelvin to explain why merge process got violated again. Background: Relates to quantify-core!156. Too many merge conflicts when Kelvin tried to update the feature branch from the latest develop in preparation for a merge to develop. He manually fixed the conflicts, but missed out on one of the lines already fixed by Victor in develop. Subsequent approval process was "ignored" and the result was the prior fix by Victor was removed.
  - People other than maintainers to review code to prevent mistakes
  - Maintainers can make small changes of their own
  - Merge conflicts should be fixed by person making the MR

## AOB (max 15 mins):
* https://unitaryfund.github.io/unitaryhack/
* Discuss unitaryhack issues! https://gitlab.com/quantify-os/quantify/-/issues/11
  - Loops and classical logic for scheduler
  - Interactive data browser
  - Put unitary fund badge on the readme and supported by section before next release