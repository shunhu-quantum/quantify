Note: This is a process and design review meeting

Present:

## Current state (Max 30 mins):

### Quantify-core:
* Release 1.0 priorities still to be decided.

### Quantify-scheduler:
* We have released v0.3.0!

Key issues and MRs left:
* Review milestones and priorities for v0.4.0

## Extra-discussions:
* quantify-scheduler!75 discussion on design of the transmon_element module.
  - Discussions on design principles, including SOLID
  - Discussions on balance between prototyping and high-level design
  - Prototyping currently done without much design consideration, then ported to main repo in MR
  - More discussions on design needed between prototype and MR
  - Separate design meetings for specific components
  - Write down designs before implementing
  - In MRs, explain reasoning behind design choices for people with different backgrounds
  - Sort out major issues before small details
  - Design features should be discussed in issues before being implemented in MRs
* Process to merge (Kelvin trusted the discussions to be actually resolved).
  - Discussion on when to merge an MR
  - Use labels or 'Draft:' to indicate status
  - Create issue https://gitlab.com/quantify-os/quantify/-/issues/16 to discuss
* How closely coupled we want Quantify to be with respect to QCoDeS? Relates to discussion with quantify-scheduler!70 on the concerns brought up by Thomas regarding complexity and the lack of our own understanding of what QCoDeS station does.
  - QCoDeS provides many useful concepts (e.g. Instruments, Parameters)
  - Widely used framework
  - Downside: fundamentally single-threaded
  - Quantum internet customers need to coordinate across different locations
  - This will be blocking in future
  - Keep coupling to QCoDeS light so we can eventually replace it
  - Create an issue on this


## AOB (max 5 mins):
* TBD