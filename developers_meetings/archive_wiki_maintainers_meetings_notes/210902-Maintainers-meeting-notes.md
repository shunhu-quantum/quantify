## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [x] Luis
- [x] Damien
- [x] Victor
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [x] Jordy
- [ ] Adam
- [x] Diogo

## Current state (Max 30 mins):

- Quantify-core [0.5.0](https://gitlab.com/quantify-os/quantify-core/-/releases/0.5.0) and Quantify-scheduler [0.4.0](https://gitlab.com/quantify-os/quantify-scheduler/-/releases/0.4.0) released 🎉🎉🎉🍻
    - Mind namespace breaking changes.
- Docs now use `sphinxcontrib-bibtex` for references with a bibliography file like in LaTeX.
    - Use it to link to papers, etc..
    - Concern: too many Sphinx extensions?
        - For now we are ok.
- Slack channel `#software-for-bots` made private to avoid leaking confidential GitLab issues.
    - Consulting a search engine did not yield better solution, we keep it private.
    - @ Project Owners who is allowed in the private channel? same people that can see the issues on GitLab?
        - For easy management: the people that are core developers have access to that channel.

- Interest in the video mode from Michiel Haye (Software architect @ TNO).
    - Provide support if needed.

- Make MM public
    - Approved.
    - Suggestion by Victor:
        - We will use Google Meet for simplicity, link managed by a Project Owner: Jules.
        - Meeting link to be published in Slack channels in a pinned message.
        - In the README/docs of both repos we will publish the Slack workspace. (Victor)
    - Update: Issue with Slack invite links: https://gitlab.com/quantify-os/quantify/-/issues/18

### Quantify-core:

- Windows CI fixed (quantify-core!214).
    - Reminder: Windows CI is not always executing with same pip packages' version as the rest of the jobs.
- New quantify dataset under design (quantify-core#187, quantify-core#233, quantify-core!208, quantify-core!212).
    - Victor back from vacation will pickup this again next week.

- Considerations for Quantify-core release 1.0.0
    - About time for new release, core is more mature than scheduler.
    - New dataset needs to be part of that release.
    - Deprecation policy, backwards compatibility, version support, etc. need to be established before release.
    - Break things that need to be broken before the 1.0.0 release.

### Quantify-scheduler:

Important merged MRs to note:

- Qblox RF module support added (quantify-scheduler!158).
- Qblox back end binning and averaging support + `qblox-instrument` version bump to 0.4.0 (quantify-scheduler!143).
- Interface definition for `InstrumentCoordinator` (quantify-scheduler!177).
- Temporary fix for `ScheduleAcqGettable` to work with binning (quantify-scheduler!168).
    - Damien comments on status?
- `DCCompensation` Pulse added to support spin schedules (quantify-scheduler!183).

Other updates:

- Windows (nightly) CI jobs failing due to 1h timeout (quantify-scheduler#186).
    - To be solved when Kelvin is back.
    - Update: might have been solved in the meantime by Luis, but a docker-based solution would still be nice.
- Luis working on mypy integration in CI (quantify-scheduler!181).
    - Status?
        - Pretty much ready.
        - But merge conflicts expected with quantify-scheduler!180.
        - It should be merged after quantify-scheduler!180 as Qblox has deliverables in a few weeks.
    - Mirror MR for quantify-core?
        - Luis working on that as well.
- Adriaan measured in the lab using Qblox binning mode 🥳🥳🥳
    - (Draft) Big MR to support binning, binning append mode, and much more opened (quantify-scheduler!180)
    - ScheduleVectorAcqGettable is now a generic **single-qubit** schedule gettable.

## Requested discussions:

- From quantify-scheduler!185 it is clear that we are missing an extra compilation layer for dc/crosstalk and timing corrections/compensations.
    - We need a proper generic way to deal with this but it is out of scope at the moment and we need to support some early usages.
    - Decisions:
        - Insert extra function in qcompile taking the schedule and one of the configuration files as input.
        - Allow the extra function to be skipped when not needed/information not provided.
        - We figure out later how to deal with this better.

## AOB (max 5 mins):

-

## Pending topics (for next MMs):

- (Requested by Adriaan) Suppress warning in load_settings_onto_instrument when trying to set parameters to None (quantify-core#232).
- (Requested by Damien) Suggestion for restructure of pulse/acquisition libraries (quantify-scheduler#159)
- (Requested by Damien) Qblox control stack component produces exception when instrument is not used in experiment (quantify-scheduler#140)
- (Suggested by Adriaan) nightly releases
- (Requested by Slava) rename/restructure branches
- (Requested by Slava) use our own CI fleet (custom runners)
- (Requested by Victor) move the meeting notes to the `quantify-os` repo to edit notes locally and version control it, MR created: https://gitlab.com/quantify-os/quantify/-/merge_requests/1
- (Requested by Victor) pending implementation: ship tests with pypi
    - Kelvin did you pick this up a while ago?
