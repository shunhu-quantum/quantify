Present:

## Current state (Max 30 mins):

### Quantify-core:

* Ready for release 0.5.0 (quantify-core!206)

### Quantify-scheduler:

* Ready for release 0.4.0 (quantify-scheduler!169)
  - Unpin dependence on develop branch
  - Changes to changelog, add to MR by end of the day

## Requested discussions:

* Schedule debugger.
  - User should be able to read schedule, not possible to get it at the moment
  - Verify that you are sending the intended pulses
  - Add self.schedule in ScheduleGettable
  - Have compiled instructions accessible too
  - Write all info to disk
  - Separate quick implementation to do in short term, with fully optimised version
  - How do you minimise data usage?
  - Decision: add property to schedule gettable, containing last compiled schedule and instructions (and document)
  - Decision: Find proper way to retrieve data
* Make Slava as one of the maintainers too.
  - Agreed to do this

## AOB (max 5 mins):

* Qblox backend: timing issues lead to side effects
  - Real time modulation: phase of pulse depends on timing of pulse, you need pulse times to be multiples of LO period.
  - Timings need to be multiples of 4ns. 
  - Catch these problems in the backend
  - Create an issue on this
  - Better error message
  - Get list of all relevant constraints