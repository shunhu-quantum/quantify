People present: Kelvin, Jules, Damien, Thomas

Core is ready for usage, remaining issues are not problematic.
Scheduler issues need attention but its better to do this next week when issue owners are back.
