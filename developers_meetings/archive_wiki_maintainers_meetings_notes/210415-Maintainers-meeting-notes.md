Present:

## Current state (Max 15 mins):

### Quantify-core:
* Documentation for analysis framework [done?]

Issues left:
quantify-core#181
quantify-core#180
quantify-core#177
quantify-core#172
quantify-core#165
quantify-core#161
quantify-core#152
quantify-core#136


### Quantify-scheduler:
* MR quantify-scheduler!81
* Zhinst backend done
* quantify-scheduler!77 is ready?

Issues/MRs left:
* quantify-scheduler!81
quantify-scheduler#36
quantify-scheduler#44
quantify-scheduler#48
quantify-scheduler#88
quantify-scheduler#101


## Discussions:

### Quantify (max 15 mins):
* (Raised by Adriaan) Modules namespace and third-party tools quantify-scheduler#51
    - [from prev. meeting] Solution not clear, BUT it will be a MAJOR breaking change.
        - Still under investigation, for a better solution.
        - Thomas found a link: https://stackoverflow.com/questions/31565932/python-namespace-package-as-extension-to-existing-package

### Core release 0.4.0 (max 15 mins):
* Potential conflict with todos: "utilities to load quantities of interest and processed dataset from each analysis folder (implement as class methods). this will make easy to load results of an analysis by passing in just a tuid." and "accept dataset as input [see this comment]. Issue quantify-core#181 spawned."
* Issue quantify-core#181 design and approach
* What are we going to do with ufloats and the jsonencoder? quantify-core#152
* quantify-core#135 - Data handling utilities: Prototype. Where? Time to refine this issue

### Scheduler release 0.3.0 (max 15 mins):
* quantify-scheduler#36, quantify-scheduler#44 (What refinements are necessary to get it to workable)


## AOB:
* Request to assign yourself in the gitlab issues whenever you're working on it. Else it is difficult to know if an issue is currently being worked on.