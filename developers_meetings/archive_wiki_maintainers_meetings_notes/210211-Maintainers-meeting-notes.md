
## Agenda
* Big issues and version labels
* Pieter issues brought in by victor
* Analysis class
* Measurements protocol
* Release 0.3.1
* Damien working on controlling Backend of qblox hardware
	
### Big issues and version labels

Subject of: Adriaan  

* We've introduced release strategy
  * Misc (0.0.x) contains small fixes
  * Minor (0.x.0) contains new features
  * Major (x.0.0) contains breaking features
* New Version Labels
  * Not all issues have version labels
    * Key features or issues have the label the `key`
    * Version labels give key features priority
    * The key feature for a version are required to have been solved in order to bump the version.

Decisions  
* Jules and Adriaan will be responsible for the "key" labeling.


### Pieter issues brought in by victor

Subject of: Victor  
Question: How to deal with issues of new contributers?  
Answer: (Adriaan) If the issue creator is a paying customer it should have higher priority to fix. In any other case it should fit within the roadmap of the Quantify Consortium.  

### Release 0.3.1

Subject of: Adriaan  
* Contains allot of small fixes
* Release is delayed due to bandwidth issues.

Decisions  
* Will be released before the end of this week.

### Analysis class

Issue: #63</br>
Merge request: !89</br>
Subject of: Adriaan</br>

* Analysis class is for 1D and 2D plots
* Some of the developers have already reviewed the merge request
* Adriaan asked everyone to have a look at it.

* Design and implementation of data structures and how to store them has been resolved and merged.
* Victor has been exploring Analysis data flow.
* The task might be solved within 2 weeks.

TODO  
* How to store data for multiple file analysis
* Add documentation

Decisions  
* Damien should start using Analysis as a beta tester and report on what is missing.

### Measurements protocol

Subject of: Adriaan  
Issue: #63  
Merge request: !51  
Protocol addresses "How to deal with measurements.", which is defined by Adriaan en Jules.  

* Adriaan mentioned what problems need to be solved before we can move on with this issue. #63
* Jules added measurement lib with measurment protocols

Decisions  
* In order to understand better how it works and what is required Jules should add some examples.


### Backend qblox hardware

Subject of: Damien  
How to handle getting data from the qblox hardware using Gettable and Settables in a generic hardware agnotic way.  
Discussion ended because the time was up.  

Solution  
Define a meta instrument which will control the qblox hardware given the compiled schedule.  

Decisions  
Damien should create merge request so that the engineers can give their feedback.  
