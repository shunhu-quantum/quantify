Present: Adriaan, Jules, Damien, Thomas, Victor, Adam

## Current state (Max 15 mins): 

### Quantify

* Added scheduled pipeline (nightly)
    - developers were informed


### Quantify-core:
* CI and docs updates mirroring latest scheduler changes 
quantify-core!136 (extra discussion for end of meeting)
* set_datadir does not check for valid paths
quantify-core#167
    - low priority (no bandwidth)
* Plot monitor crashing for very large scans
quantify-core#168
    - low priority (no bandwidth), solutions proposed
* utilities.experiment_helpers.load_settings_onto_instrument does not work in practical setting quantify-core#166 (Adam has enough info I suppose?)
    - Adam working on it as bandwidth permits
* Adaptive plotting crashing quantify-core#61 (7 months, no minimal example to reproduce)
    - low priority (no bandwidth), the issue expected to still be there, to be refined
* Add more tests for plotmon (quantify-core!85) quantify-core#130 (Still opened or fixed?)
    - still opened, low priority, important for quality assurance, our developers noted that some their changes might break plotting (spotted by some tests)


### Quantify-scheduler:
* Acquisitions protocol quantify-scheduler!51 (Finally!). The new issues?
    - New issues created, missing things for a next release in the comments of the MR
* Example spectroscopy programs quantify-scheduler!64
    - Important to have as testing examples of the full stack
    - A time domain schedules MR opened as a follow up


## Discussions:
### Urgent discussion (max 15 mins):

* (Raised by Victor) Decide on docs dependencies and docstrings format quantify#10
    - On the big picture, the major issue we are facing are due to quantify dependencies.
    - We identified that the dependence on external backend would significantly slow down the python loading that could be avoided by the use of the `TYPE_CHECKING` flag
    - Thomas volunteered to have a loot at the issue with qcodes and plotly
    - We also noted some dissatisfaction with the plotly among some of our developers.
    - It is also not very clear what is this sphinx dependency interaction with some of the sphinx's built-in functionalities (https://github.com/agronholm/sphinx-autodoc-typehints/issues/167)

### Quantify (max 15 mins):

* (Raised by Adriaan) Modules namespace and third-party tools quantify-scheduler#51
    - Adriaan explained the problem, most of us agree with its impact.
    - Solution not clear, BUT it will be MAJOR breaking change.
* (Raised by Victor) Copyright chores in quantify repos python files quantify-scheduler#92
    - Adopted, MR to be created + tests
* (Raised by Victor) Do not allow MRs to merged if the docs raise fixable warnings quantify-scheduler!73
    - Adriaan disagrees because the checklist is already too big
    - Discussion postponed

### Quantify-core (max 15 mins):
* Other technical issues raised?

### Quantify-scheduler (max 20 mins):
* Progress on 0.3.0 (priorities changed to 2 functional backends last meeting)
    - We agree 0.3.0 to include both backends.
    - Other things are being developed in parallel, most likely will be shipped as well due to the difficulty in filtering released features.
* Other technical issues raised?

## Any Other Business (5 mins):
* (Raised by Victor) quantify-scheduler lacks code and essential documentation, e.g. difficulty in reviewing quantify-scheduler!64
    - Not discussed