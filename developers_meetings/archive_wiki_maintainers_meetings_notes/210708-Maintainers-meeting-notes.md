Present:

## Current state (Max 30 mins):

### Quantify-core:

* xarray >= 0.18 is now compatible
  - Don't need to pin versions anymore
* Fully using the docker testing infrastructure

### Quantify-scheduler:

* Docker test infrastructure is also fully merged.
  - Contains pre-installed version of dependencies
* ZI backend tested to work. But need discussion on LocalOscillators InstrumentCoordinatorComponent design and implementation. Still in progress.
  - Kelvin just finished testing the new name change from controlstack to instrument_coordinator.
* Add numerically defined pulses to Pulse Library. Specifically the function `make_list_from_array`. Where and what is blocking? quantify-scheduler!157
  - Flag as blocked due to internal reasons

## Requested discussions:

* Gettables examples quantify-core#208 quantify-scheduler!109
  - Have stopwatch gettable in core and schedule gettable in scheduler (now possible due to namespace change)
* Stale MRs and issues: (Definition of Quantify dataset) quantify-core#187, quantify-core#139, quantify-core!111
* Time for new releases. Especially with the namespace change in develop.
  - New concepts have been introduced, design not final. We would have to make breaking changes later
  - Quantify core ready for release, scheduler not ready yet.
  - Product owners to come up with priorities for release as soon as possible
* Should we open up our maintainers meeting to the public as recommended by UF?
  - Good for open source project
  - Proprietary products are sometimes discussed in meetings
  - Harder to keep discussions on track
  - Maybe have Damien and Slava as maintainers
  - Approve decision, check with Jules

## AOB (max 5 mins):
* UnitaryFund decided to proceed with the second half of our "project", so, they're transferring the remaining USD 2K to us.
