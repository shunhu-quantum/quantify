## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Luis
- [x] Damien
- [x] Victor
- [ ] Slava
- [x] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [x] Jordy
- [x] Adam
- [x] Diogo
- [x] Gijs

## Introduction new member:
- Maybe Amber (OrangeQS lead business developer) might join halfway.

## Highlights:
- Dataset v2.0 Spec almost ready.
- Quantify-core dataset, see discussion in slack and gitlab [quantify-core MR 224](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224), [quantify-core RTD MR 224](https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/dataset_design/index.html).

### Quantify-scheduler:
- Repetitions variable `cfg_nr_averages` changed to `cfg_sched_repetitions`. [quantify-scheduler MR 205](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/205)
    - Propagates to OQS repos.
- No longer need to set repetitions keyword in the sched_kwargs as it is also filled in by the `ScheduleGettableSingleChannel` during the call to the schedule_function.
- MRs related to ZI backend [Fix for frequency definitions: quantify-scheduler MR 200](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/200) [Always recompile for ZI backend quantify-scheduler MR 202](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/202)


### Quantify-core:
- Milestone review (V0.6, V1.0)
- Almost finished.
- Quantify-core dataset, see discussion in slack and gitlab [quantify-core MR 224](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224), [quantify-core RTD MR 224](https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/dataset_design/index.html).
    - By default all attributes will be serialized to JSON. Attribute will allow to skip serialization for designated attribute.


## Requested discussions:


## AOB:
- (Requested by Kelvin) unitary fund final check in/demo
    - Propose to use the tutorials from core, and scheduler (maybe if possible I will use an emulator from OrangeQS).
        - We don't need to show scheduler necessarily since it is not in the scope of the project.
    - Mention that we are also busy re-working the dataset. (if bandwidth permits, I will use the sample advanced usecase notebook).
    - Mention our future plans for release of core 1.0, and scheduler.

- Biweekly MM instead?
    - No, for now it seems ok to have shorter meetings.

## Pending topics (for next MMs):
- Update on the UF demo
