## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [X] Luis
- [X] Damien
- [X] Victor
- [ ] Slava
- [X] Adriaan (PO)
- [X] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [X] Diogo
- [ ] Gijs
- [X] Damaz

## Introduction new member:

## Highlights:
- Quantify-core dataset v2 specification is ready for merge.
- Quantify-scheduler has another external contribution merged!

### Quantify-scheduler:

- ZI Backend
    - SSB demodulation support and other bugfixes (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/227)
    - UHFQA acquisition weights (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/234)

- Qblox Backend
    - Mixer corrections units (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/233)

- There are test programs available (by Adriaan)
    - Should be used to check that both backends work correctly.

- Missing LO ICC or equivalent
    - Hacky versions used by OQS/Qblox
    - Old MR is there, too outdated by now
    - Should be part of quantify-scheduler
    - Adriaan to follow up at some point since he needs it anyway.
    - Damien can help with future MR review.

#### Merged MRs (highlights)

- WindowOperation by Pieter (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/232)
    - Kelvin has to explain what this feature was about next MM.

### Quantify-core:

#### v0.6 milestone

- Milestone board: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Target: End of October

- Dataset spec finished this week, Ready for merge.
    - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224
    - https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/dataset_design/index.html
    - Deadline for last reviews before merge button is pressed: Friday evening, 22nd Oct.
- Very important inputs required as soon as possible:
    - Dataset v2 implementation draft (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/243)
- Dataset validation might be a bit of a rabbit hole.
    - Props to Luis for researching this.
    - Deciding which method/package(s) to use might be difficult without actually using them.
    - Victor will create new dedicated issue for a dataset v2 validator to allow progress on the rest of the MRs and issue.


#### Merged MRs (highlights)
- Hotfix to the CI relating to lmfit and filelock (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/242)


## Requested discussions:

- Release in pypi possibly delayed to mid next week (latest 29 October). Reason:
    - Would like to include a fully working ZI backend for staircase test.
    - Kelvin needs to take an urgent leave this Friday.
    - Decision from MM:
        - More releases is better for clients.
        - We can have two sequential releases (a minor and a misc).
        - Damien can get started with a MR for the new release and Adriaan can help with the PyPi.

- (Requested by Victor) Agree on procedure for hacks. Branching practices for example.
    - Issue acknowledged. A bit of process was overlooked.
    - Be more careful next time.

## AOB:
-

## Pending topics (for next MMs):
-
