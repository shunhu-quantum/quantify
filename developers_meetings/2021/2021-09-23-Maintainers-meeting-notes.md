## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [ ] Damien
- [ ] Victor
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs Vermariën (OQS intern, welcome!)

## Introduction new member:

## Highlights:
- External contributions merged (thanks to Victor).
- Thanks to Pieter Eeendebak [quantify-core MR222](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/222)
- Thanks to Harold Meerwaldt [quantify-core MR226](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/226)

### Quantify-scheduler:
- Milestone review

### Quantify-core:
- Milestone review (V0.6, V1.0) - Victor to help explain.

## Requested discussions:
- Quantify-core dataset, see discussion in slack and gitlab [quantify-core MR 224](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224), [quantify-core RTD MR 224](https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/index.html)


## AOB:
- (Requested by Slava) rename/restructure branches
    - Suggest combining master and develop into main
    - Maintenance branches as well
    - Decision approved, create issue to follow up later

- Next meetings allocate time for external contributors/participants since the meeting is public now.

## Pending topics (for next MMs):

