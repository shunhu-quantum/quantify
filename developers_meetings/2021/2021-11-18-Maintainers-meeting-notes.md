## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [ ] Damien
- [ ] Victor
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz

## Introduction new member:
- Michiel (Interns from OQS)

## Highlights:
- Breaking changes for `develop` branch of quantify-scheduler. `qcompile` uses `hardware_cfg` now instead of `hardware_mapping`. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/279) 

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
- ZI backend working with known constraints. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/263)
    - Work on cleaning up the MR has started. Expected finish on Wed next week.
    - Some features e.g. error handling useful for qblox backend as well.

#### Merged MRs (highlights)
- Cached locate for operation deserialization. Speedup for python >= 3.8 (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/273)
- The `determine_absolute_scheduling` function now sorts the list of labels in the timing constraints, and then a binary search (via `np.searchsorted`) is applied. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/274)
- Add method to plot acquisition operations. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/271)


### Quantify-core:
- Matplotlib pinned to < 3.5 (Problems with set_clim). (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/269)


#### Merged MRs (highlights)
-

## Requested discussions:
- (Requested by Damaz) Pre-commit is horrible for first time users! Only need isort and black?
    - We will eliminate all pre-commits except black
    - Gijs and Michiel to implement

## AOB:
- Assign dataset V2 implementation to Slava

## Pending topics (for next MMs):
-
