## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj

## Introduction new member:

## Highlights:
- Quantify-core 0.5.2
- Quantify-scheduler 0.5.2

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
- Multiple acquisitions for schedule gettable in progress. Verify with Adam to see if Analysis will be a problem (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/299)
- Breaking changes (we now use `fastjsonschema` instead of `jsonschema` package) (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/293/)

#### Merged MRs (highlights)
- Qblox downconverter backend support. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/297)
- Initial LocalOscillator common schema is implemented. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/283)

### Quantify-core:
- Approximately 1 month left (https://gitlab.com/quantify-os/quantify-core/-/milestones/9)\
  - #254 more-or-less done
  - Start on #259 and #255
  - Slava to do #255, Kelvin #259

#### Merged MRs (highlights):
- QuantifyExperiment merged (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/274)

## Requested discussions:

## AOB:
- Reminder: Next week's developers meeting is on the 17-Dec (11 am NL time).

## Pending topics (for next MMs):
