## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [x] Damien
- [x] Victor
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [ ] Jordy
- [x] Adam
- [x] Diogo
- [x] Gijs
- [x] Amber
- [x] Damaz de Jong
- [x] Bektur Murzaliev

## Introduction new member:

- Damaz de Jong - recently joined Qblox, experimental physics background 🎉
- Bektur Murzaliev - new Quantify use, post doc TU Delft 🎉

## Highlights:

- Unitary Fund Grant: Quantify Final Check-In
    - Kelvin presented some quantify-core tutorial and we made a good impression.
    - Kelvin might follow up with them on Docker images vs USB support.
    - Amber will follow up on the Quantify marketing that the Unitary Fund folks are happy to help us with. They consider it very valuable for the experimentalist to know about Quantify.
- Adriaan gave presentation on Quantify in Zurich, quantum industry day.

### Quantify-scheduler:

- All `priority::key` labels removed.
    - To be used again when the scheduler milestones are reviewed at some later point.

#### Merged MRs (highlights)

- Add method to sample Schedule
    - Contribution by Pieter Eendebak 🎉.
    - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/212

    - Other MR contributions from Pieter:
        - Allow user defined axis for plotting circuit diagram (!206).
        - Set default clock for the square pulse (!207).
        - Add offset parameter to ramp pulse (!211).

- ZI Backend
    - Several hotfixes and hacks.
    - Forced by external deadlines, affects code quality and package structure.
    - Might require review of the InstrumentCoordinator architecture (there is an issue on redesigning this).

- Qblox Backend
    - Qblox new firmware support to be merged soon (likely next week).
    - Several MR reviewed and merged into a separate branch awaiting for firmware and qcodes drivers release.
        - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/214
    - Adriaan proposes standardized testing on real hardware for major new upgrades.

### Quantify-core:

#### v0.6 milestone

- Milestone board: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Target: End of October

- Dataset specification to be finished this week, final polishing (Victor).
    - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224
    - https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/dataset_design/index.html
- Xarray subclass prototyping started (Luis).
    - https://gitlab.com/quantify-os/quantify-core/-/issues/254

#### Merged MRs (highlights)

- Added lazy set functionality to measurement control (iterative loops)
    - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/233
    - Contribution from Harold Meerwaldt 🎉.
    - Saves communication with instruments during experiments.
    - Not working yet for batched mode (there is an issue on this)

- Detect calibration points automatically for single qubit time-domain experiments
    - Failing fits due to wrong assumption about calibration points should be fixed now.
    - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/234
    - https://quantify-quantify-core.readthedocs-hosted.com/en/latest/api_reference.html#quantify_core.analysis.calibration.has_calibration_points


## Requested discussions:

- (Requested by Victor) Victor is leaving Qblox and the Quantify maintainer role.
    - Priority: Finish the dataset specification and as much progress as possible for the v0.6 milestone.
    - Suggestion:
        - Maybe write the docs/docstrings/sphinx/type-hinting guidelines?
            - Agreed Victor will do this.
            - Also issue on docs not building.
        - Will you really read and apply it when reviewing MRs?
    - What are the knowledge transfer needs of the rest of the maintainers?
        -

- (Requested by Victor) Status on the issues marked as `Critical` related to ZI backend?
    - 1 week+ old, still considered Critical?
    - Adriaan: Cannot answer right now, as Kelvin not in meeting
    - Perhaps: 'critical' part of issue solved, but issue not fully resolved.

## AOB:

- Issue with installing quantify
- Get rid of 'stable' and 'latest' designations on docs. Refer only to numbered versions and branch names. Readthedocs to point to latest released version by default.

## Pending topics (for next MMs):
-
