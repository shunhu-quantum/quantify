## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj

## Introduction new member:

## Highlights:

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
- Multiple acquisitions for schedule gettable in progress. Verify with Adam to see if Analysis will be a problem (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/299). Still pending.
- Tutorials key: (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/241)
- Are these still issues?
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/240
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/243

#### Merged MRs (highlights)
- Qblox backend - basic support for generic icc (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/306)
- Trace operation fix (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/300)

### Quantify-core:
- Approximately 1 month left (https://gitlab.com/quantify-os/quantify-core/-/milestones/9)\
  - #254 more-or-less done
  - Start on #259 and #255
  - Slava to do #255, Kelvin #259

#### Merged MRs (highlights):

## Requested discussions:

## AOB:
- (Requested by Kelvin) Christmas eve developer meeting? Pause meetings and resume on 7th Jan?

## Pending topics (for next MMs):
