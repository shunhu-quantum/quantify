## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Damien
- [x] Slava
- [x] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [x] Jordy
- [x] Adam
- [x] Diogo
- [x] Gijs
- [x] Damaz
- [ ] Vraj
- [x] Michiel

## Introduction new member:

- [ ] Tobias
- [x] Achmed

## Highlights:


### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
  - Nano-NISQ is high priority at the moment, so a lot of work is been done on that.
  - 0.5.x is almost done

#### Merged MRs (highlights)
-  Removal of ic prefix from hardware config (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/312) {Confirm if breaking}
  - Must be updated in changelog, since it is breaking.

### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Deprecation policy 
  - Please take a look -> Do we need a deadline for merge? - Will set deadline after Developers meeting Jan 14th
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/282
  - Damien and Kelvin will take closer look at MR today.
- Experimental features
  - Should we add decorator for experimental features? Yes.
  - For new/replacement functionallity which are used, but still under development.
  - Should be added to deprecation policy at some point.
  

#### Merged MRs (highlights):
- Multi experimental data extraction and data concat (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/278)


## Requested discussions:

- Rename default branch to main
  - https://gitlab.com/quantify-os/quantify/-/issues/12
  - Adriaan: scared about side-effects, but decide to do it
  - Damien: need someone to take the lead, Adriaan: one of the maintainers should do this - decide next week
  - Slava will take the lead for this.

- QuantumDevice setup from device_config (Diogo Valada):
  - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/315
  - Please everyone take a look at this
  - Adriaan: for transmons it is natural to use current structure with device config and hardware config. For other types of qubits this might be different, because they are not as mature.
  - Adriaan: There is a bigger breaking change coming (new more general quantum device compilation/config), so do we want to go forward with this smaller breaking change? Since both break the same thing and this MR is a smaller improvement.
  - Have both old and Diogo's version supported and give deprecation warning to the old config. Diogo will take this into account.
  - Diogo will move forward with the MR.

## AOB:
- Problem with plotmon
  - https://gitlab.com/quantify-os/quantify-core/-/issues/285
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/271
  - Most likely problem with multiprocessing.
  - Slava: plotmon should be rewritten to different plotting library. However, no one wants to touch this.
  - Kelvin: needs help with debugging the windows machine.
  - Issue is not critical, no one has the time to pick this up. 
  - Fix is not possible to implement. Michiel will look at the MR. 

## Pending topics (for next MMs):

