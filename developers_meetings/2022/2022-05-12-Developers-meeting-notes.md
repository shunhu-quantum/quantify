
## Highlights
_None_
  

## Breaking changes
- `add_component`, `get_component`, `remove_component`, `components` functions and submodules are deprecated. (quantify-scheduler!389)


## Quantify-scheduler
- Pipeline fixed
- Edge object implemented in QuantumDevice for device config generation. (quantify-scheduler!389)
  

#### Merged MRs (highlights)
- Add edges in quantum device (quantify-scheduler!389)
   

## Quantify-core
_None_
  
#### Merged MRs (highlights)
- Mark test_flow_clim_all xfail (quantify-core!341)
- Unpin setuptools version (quantify-core!312)
- Update CONTRIBUTING.rst (quantify-core!339)
  

## Requested discussions / Any other business
_None_


## Pending topics (for next DMs)

