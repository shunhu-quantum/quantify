## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj
- [ ] Tobias
- [ ] Achmed

## Introduction new member:

## Highlights:

- Docs issue solved temporarily. QCoDeS version pinned to <0.32.0 again in both repos.
- **_Follow up:_** *Performance benchmarking of init & connecting to instruments*
  - _Create separate tickets for `core` and `sched`_
  - _OQS to supply mock setup_

## Breaking changes:

- Gettables - `ScheduleGettable` now first stops all instruments in IC during initialization (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/324)

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)
- *Damien went over issue list and closed a few already resolved items -- lots of issues though so timely to check everything*
- **_Idea:_** *specifically for issues listed under milestones, check for already resolved to clean up the milestone boards?*
- *Heads up: Scheduler product backlog session, after APS March, 1-2 days*

#### Merged MRs (highlights)

- QuantumDevice - Unknown values are initialized as `float('nan')` (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/356/)
- Qblox ICCs - Stop now disables sync on all sequencers to prevent hanging during next run, where it gets re-enabled if needed (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/324)
- Qblox ICCs - Fixed bug where QRM scope mode sequencer does not get set correctly (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/342/)
- Qblox backend - NCO phase now gets reset every averaging loop (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/337)
- Zhinst backend - Fixes bug when doing SSRO experiments. No more duplicated shots. Adds support for BinMode.APPEND during compilation. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/358)


### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
  - Status data backends https://gitlab.com/quantify-os/quantify-core/-/merge_requests/303?
  - _Make sure to review new MRs before merging into the https://gitlab.com/quantify-os/quantify-core/-/tree/sprint_dataset_v2 branch so we keep it easily digestable_ 


#### Merged MRs (highlights):

- Allow `test_basic_1d_plot` to fail on Windows (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/306)
- Measurement control - Add option to not save data in MC (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/308)

## Requested discussions:

- Google meet access rights  
*Zoom link via which anyone can join, thanks to Tobias: https://tudelft.zoom.us/j/95515974195?pwd=SnlFZTFIRHB1U2R4OGhGdFk5M3hnUT09*
- Plan next releases `core` and `scheduler`  
_`core`: misc release asap, Damien will pick this up next day_  
_`scheduler`: minor release after fix bug on the Zurich backend https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/359 (early next week)_


## AOB:

- Open the floor.  
*Potential plotmon replacement: Dask JupyterLab extension that opens windows as tabs: https://github.com/dask/dask-labextension*

## Pending topics (for next MMs):

- No pending topics
