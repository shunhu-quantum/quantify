## Highlights
_None_

## Breaking changes
- Qblox ICC - `ClusterComponent.prepare` does not mutate schedule anymore. (QAE-395) (see quantify-scheduler!443)
- Qblox ICC - Fixes and changes to the behaviour of the QRM, QRM-RF. (QAE-305) (see quantify-scheduler!432)

## Quantify-scheduler
_None_

#### Merged MRs (highlights)
- New `trace_schedule_circuit_layer` added. This generates a simple schedule at the circuit layer to perform trace acquisition. (quantify-scheduler!442)

## Quantify-core
- Proposed deprecation policy is formalized and merged. (quantify-core!282)
- Release 0.6.3 to be drafted and planned for before the dev meeting next week.

#### Merged MRs (highlights)
- Fix for race conditions in the plotmon and instrument monitors. (quantify-core!358)


## Requested discussions / Any other business

### Public API
We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**: 
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

Include new checkboxes on MR?
- Changed functionality: public API so should we deprecate? => checkbox: n/a, done
- New functionality: should it be private? => checkbox: n/a, done
- Should be tested on hardware? => checkbox: n/a, done

**Decision**:
- Everyone is welkome to improve template by sending an MR, but probably this will be done by maintainers.
- "Manual test" checkbox is needed.


### Documentation
_Edgar: propose to move this to next week, some of the math statements aren't rendering (https://quantify-quantify-scheduler--349.com.readthedocs.build/en/349/autoapi/quantify_scheduler/operations/gate_library/index.html#quantify_scheduler.operations.gate_library.X90)_
There is an open merge request related to adding documentation to gates (see quantify-scheduler!349).

### Line lengths
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Extra discussion
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler. 
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation. 



## Pending topics (for next DMs)
_None_
