## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Damien
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [x] Jordy
- [ ] Adam
- [x] Diogo
- [x] Gijs
- [x] Damaz
- [x] Vraj
- [ ] Tobias
- [x] Achmed

## Introduction new member:

## Highlights:
- Another external user (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/260)

## Breaking changes:
- Default branch for both repositories are now "main".
  - Thank you Slava!
- The ic_ prefix issue does have a simple solution (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/312). Change all hw config to without the "ic_instrument_name".

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
  - Quite behind, but focus is on quantify-core.

#### Merged MRs (highlights)
- Generic ICC can take in nested parameters (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/330).
- ZI UHFQA backend no longer generates csv files with name prefixed with "ic_" (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/334).

### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Deprecation policy 
  - Remove draft status?
    - Slava did not merge proposed changes, so not yet.
    - Can be implemented during sprints.
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/282
- Quantum-circuit to quantum-device layer compilation backend
  - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/339
  - A presentation will be done in some time, but the functionallity is quickly necessary.
  - A new qubit object / changed qubit object is necessary before it works.
  - Adriaan will hold a presentation when he is back from Finland.

#### Merged MRs (highlights):
- None this week.

## Requested discussions:

- Time to merge schedulable (breaking changes)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/309
  - Test with hardware setup, Kelvin with Koln, Slava with qitt lab, if works then it can be merged.

- Pulse compilation. Already some comments have been addressed. Ready to remove draft status?
  https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/339 

- Volunteers to be in the weekly sprints in Feb to push quantify-core to 1.0?
  - Damien and Edgar from Qublox
  - Kelvin and Slava from Orange QS
  - Sprints will be on Thursday

## AOB:
- Using pydantic for configs, Slava will send an update. Adriaan will try to use it to see how easy it is.

## Pending topics (for next MMs):

