Dear fellow developers, please note that there is no meeting today as it is a public holiday in the Netherlands.

## Highlights
- Quantify-core is now in version 0.6.0!
- Release Pipeline now automatic upload to test.pypi. See Release 0.6.0 MR for more details. (quantify-core!344)

## Breaking changes
_None_


## Quantify-scheduler
_None_

#### Merged MRs (highlights)
- Fix issue with outputting real signals on even output paths (qblox hardware) (quantify-scheduler!397)
- Fix jupyter-execute in QASMProgram (quantify-scheduler!396)
   

## Quantify-core
- Release v0.6.0 (quantify-core!344)
  
#### Merged MRs (highlights)
_None_
  

## Requested discussions / Any other business
_None_


## Pending topics (for next DMs)
_None_


