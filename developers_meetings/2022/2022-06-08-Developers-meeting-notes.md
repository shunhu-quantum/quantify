## Highlights
- UnitaryHack is underway. Do remember to sign up to win 100 USD bounties (for non-OrangeQS and non-Qblox members)! (https://unitaryhack.dev/projects/quantify-scheduler/) (https://unitaryhack.dev/projects/quantify-core/)

## Breaking changes
_None_

## Quantify-scheduler
- MR (quantify-scheduler!399) still to be reviewed.

#### Merged MRs (highlights)
_None_

## Quantify-core
_None_
  
#### Merged MRs (highlights)
_None_
  

## Requested discussions / Any other business
_None_


## Pending topics (for next DMs)
_None_



