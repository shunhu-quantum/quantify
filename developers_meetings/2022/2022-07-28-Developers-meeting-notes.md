## Highlights
_None_

## Breaking changes
_None_

## Quantify-scheduler
_None_

#### Merged MRs (highlights)
- New scheduler tutorials [Schedules and Pulses; Compiling to Hardware; Operations and Qubits] (QAE-23) (quantify-scheduler!336)
- Generalize Qblox Backend for External Downconverters (QAE-134) (quantify-scheduler!418)
- Add generate_diagnostic_report method to ScheduableGettable (QAE-95) (quantify-scheduler!408)
- Call `determine_absolute_timing` in `qcompile` when no `device_cfg` supplied (quantify-scheduler!436)
- Pin sphinx to 5.0.2 due to crash in napoleon (_consume_inline_attribute) (quantify-scheduler!437)
- Qblox backend - Remove all references to the inactive `"line_gain_db"` param (quantify-scheduler!435)

## Quantify-core
_None_



## Requested discussions / Any other business

### Follow-up edge-naming
On the long term we should avoid parsing labels, in other words avoid relying on storing edges related information in names, but for now we decide to replace - with _ in edge names and disallow using _ in qubit names. 

### Adding label to instruments
Currently `InstrumentMonitor` displays ugly instrument names and it would be great if there was a `label` parameter that can be used instead. Konstantin asked the developers of QCoDeS about this and their reply was that they look forward to reviewing a Pull Request once we implement this functionality ourselves. Konstantin will pick this up. 

### Graph Compilation
Adriaan apologizes, but kindly requests more people to review and comment on the open Merge Request (see quantify-scheduler!407). It is important that everyone at least looks at it and gives an okay instead of enforcing it. Most issues have been addressed. One open issue regards the naming of "backend" as it too generic. Backend refers to a software architecture, whereas functions and methods should reflect functionality instead of architecture. 

We agree that a compiler is a graph (or collection) of nodes, where each node is one step of the compilation. One of these nodes will be hardware specific (qblox, zhinst,..). 

Subclassing each node to reflect different functionality (e.g. compile, hardware, backend,...) leads to too many subclasses and is not desirable.  

Everything is backwards compatible and, unless they look at internals, user will not notice any difference. 

Refactoring the backend/hardware to specific nodes has not happened yet. 

Please read the merge request and comment :)

### Documentation
Adriaan proposes that everytime a change is made/proposed to the documentation this should be briefly discussed/showcased as an agenda point. This shows appreciation and also triggers the right discussions. 

There is an open merge request related to adding documentation to gates (see quantify-scheduler!349). We will add this to the agenda of next week (04/07/2022) as a discussion point. 

### Other
Robert suggests using a code spellchecker to ensure documentation and function names don't contain spelling mistakes.

## Pending topics (for next DMs)
_None_
