## Highlights

### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)


## Quantify-core

### Merged MRs (highlights)
- Restore initial_value of InstrumentMonitor update_interval param (quantify-core!375)

## Requested discussions / Any other business

### Fokko: (Very) Quick follow-up to last weeks SpecPulse intro
- https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330
- Intented outcome: Just a shoutout to look at the issue and contribute to the online discussion. Stay tuned for conclusion next week!

### Adriaan: Requested discussion, what should the Hardware Config be?
- Required reading: https://gitlab.com/quantify-os/quantify-scheduler/-/issues/222#note_1091786144.
- Intended outcome: is that we share some historical context and identify what needs to be done to converge (could be what questions need to be addressed or identifying what is controversial).

### Kelvin/Kesnia: Follow up discussions on Qiskit-Quantify available
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465


## Pending topics (for next DMs)

### External MR (No follow up yet since 2022-08-11)
- Looks like Kelvin needs to review the MR on the zhinst backend. (quantify-scheduler!399)
- Related to the discussion raise since 2022-09-01
```
Zhihao:
- refactor of zhinst almost done
- How can I help reviewing
  - Will post presented slide deck (on 2022-08-25) in the issue
- Adriaan: 
  - explain ideas behind the refactor: explain design goal (Post slide deck in the issue)
  - can we break up the merge request into smaller MRs
    - per MR explicitly state what changed
  - flag anything controversial, breaking
  - hardware testing: sched contains modules for hardware testing (quantify_scheduler/schedules/verification.py)
    - include a notebook with a description with the hardware setup
```

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.


